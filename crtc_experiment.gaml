model NewModel

global skills: [network] {
    int number_of_cells <- 4;
    int number_of_devices <- 10;
    graph floor_graph <- spatial_graph([]);
    
    conveyer cleaning_conveyer;
    conveyer grading_conveyer;
    conveyer rowa_conveyer;

    init {
    	// If you have a MQTT broker you an send messages by updating your IP, port and password here and on line 17
    	// do connect to:"192.168.86.162" protocol:"MQTT" port:1883 with_name:"sender_1" login: "pi" password: "<Your broker password>";
    	
    	create device number: number_of_devices {
    		// do connect to:"192.168.86.162" protocol:"MQTT" port:1883 with_name:"device " + self.name login: "pi" password: "<Your broker password>";
    	}
    	
    	list<cell> cells <- [];
    	
    	/*
    	 * Arch paradigm is to create a number of station and connect them to a conveyer line (input and output)
    	 */
    	
    	create conveyer {
    		cleaning_conveyer <- self;
    	}
    	
    	
    	create induction_cell {
    		cells << self;
    		self.output_conveyer <- cleaning_conveyer;
    	}
    	
    	
    	
    	create conveyer {
    		grading_conveyer <- self;
    	}
    	
    	create cleaning_cell {
    		cells << self;
    		self.input_conveyer <- cleaning_conveyer;
    		self.output_conveyer <- grading_conveyer;
    	}
    	
    	
    	
    	create conveyer {
    		rowa_conveyer <- self;
    	}
    	

    	create grading_cell {
    		cells << self;
    		self.input_conveyer <- grading_conveyer;
    		self.output_conveyer <- rowa_conveyer;
    	}
    	
    	create rowa_cell {
    		cells << self;
    		self.input_conveyer <- rowa_conveyer;
    	}
    
    	// really just doing this to draw.. Not sure there's any other use.
    	floor_graph <- floor_graph add_node(cells at 0);
    	floor_graph <- floor_graph add_node(cells at 1);
    	floor_graph <- floor_graph add_node(cells at 2);
    	floor_graph <- floor_graph add_node(cells at 3);
    	floor_graph <- floor_graph add_edge(cells at 0 :: cells at 1);
    	floor_graph <- floor_graph add_edge(cells at 1 :: cells at 2);
    	floor_graph <- floor_graph add_edge(cells at 2 :: cells at 3);
    	
    	do send to:"receiver" contents: "Starting";
    }
}

// Every cell has a queue where devices wait for intake. This can also be applied to a conveyer.
species queue control: fsm {
	int size <- 1;
	list<device> queued_devices <- [];
	
	state ready initial: true {
		enter {

		}
		
		transition to: full when: length(queued_devices) >= size;
	}
	
	state full {
		enter {

		}
		
		transition to: ready when: length(queued_devices) < size;
	}
	
	bool add(device d) {
		if(state != "full") {
			queued_devices << d;
			return true;
		}
		
		return false;
	}
	
	device pop {
		if(length(queued_devices) = 0) {
			return nil;
		}
		
		device d <- first(queued_devices);
		remove from: queued_devices index: 0;
		return d;
	}
	
	// TODO: this could probably be a state
	bool isEmpty {
		return length(queued_devices) = 0;
	}
}

// Generic device. This is the object we're going to move through the stations.
species device control: fsm  skills: [network] {
	int condition <- rnd(99);
	string grade;
	
	state ready initial: true {
		enter {
			//write "Device ready " + name;
			do send to:"receiver" contents: "Device ready " + name;
		}	
	}
	
	state inducted {
		enter {
			//write "Inducted " + name;
			do send to:"receiver" contents: "Inducted " + name;
		}
	}
	
	state on_conveyer {
		enter {
			//write "Conveyer " + name;
		}
	}
	
	state queued {
		enter {
			//write "Queued " + name;
		}
	}
	
	state processing {
		enter {
			//write "PROCESSING " + name;
		}
	}
	
	state waiting {
		enter {
		//	write "waiting " + name;
		}
	}
	
	state done final: true {
		enter {
		//	write "**Done** " + name;
			do send to:"receiver" contents: "Done " + name;
		}
	}
}

// Station connectors. A concept of speed and capacity has not been implemented.
species conveyer {
	queue q;
	int queue_size <- number_of_devices; // TODO: not implemented
	
	init {
		create queue {
			self.size <- myself.queue_size;
			myself.q <- self;
		}
	}
	
	bool admit(device d) {
		bool didAdd <- q.add(d);
		
		if (didAdd) {
			d.state <- "on_conveyer";
		}
		
		return didAdd;
	}
	
	device pull {
		return q.pop();
	}
}

// Base class for all Cells. 
species cell {
	int number_of_stations <- 1;
	queue q;
	conveyer input_conveyer;
	conveyer output_conveyer;
	
	// Each cell has one -> many stations. A device is manipulated (eg. cleaned, graded) over a period of time.
	species station control: fsm {
		int counter <- 0;
		int tick_time <- 6;
		device process_device;
		cell parent_cell;
		
		state waiting initial: true {
			transition to:processing when: process_device != nil {
				process_device.state <- "processing";
			}
		}
		
		state processing {
			enter {
				counter <- 0;
			}
			
			counter <- counter + 1;
			
			transition to: waiting when: counter = tick_time {
				process_device.state <- "waiting";
				
				ask parent_cell {
					do release_device(myself.process_device);
				} 
				process_device <- nil;
			}
		}
	}
	
    aspect base {
    	draw circle(1) color: #green;
    }
    
    action release_device(device d) {
    	if(output_conveyer = nil) {
    		d.state <- "done";
    	} else {
	    	ask output_conveyer {
	    		do admit(d);
	    	}    		
    	}
    }
    
    reflex cell_queue when: q.state = "ready" {
		device d <- input_conveyer.pull();
		
		ask q {
			do add(d);
			d.state <- "queued";
		}
	}
	
	// we could also check to make sure the station is ready to receive
	reflex process when: length(q) > 0 {
		loop s over: station {
			if(s.state = "waiting") {
				s.process_device <- q.pop();
			}
		}
	}
}

species induction_cell parent: cell {	
	init {
		location <- {10,10};
		
		create queue {
			self.size <- number_of_devices;
			myself.q <- self;
		}
		
		loop d over: device {
			ask q {
				do add(d);
				d.state <- "inducted";
			}
		}
	}

	reflex process when: q.isEmpty() = false {

		if(output_conveyer.q.state != "full") {
			bool admitted <- output_conveyer.admit(q.pop());
			
			if(admitted = false ) {
				write "Something went wrong";
				do die;
			}
		}
	}
	
	reflex cell_queue when: q.state = "ready" {}
	
	aspect default {
		draw square(10) color: #green border: #black;
	}

}

species cleaning_cell parent: cell {
	init {
		location <- {30,10};
		
		create queue {
			self.size <- 3;
			myself.q <- self;
		}
		
		create station number:2 {
			self.parent_cell <- myself;
		}
	}
	
	aspect default {
		draw square(10) color: #gray border: #black;
	}
}

species grading_cell parent: cell {
	init {
		location <- {50,10};
		
		create queue {
			self.size <- 3;
			myself.q <- self;
		}
		
		number_of_stations <- 2;
		
		create station number:number_of_stations {
			self.parent_cell <- myself;
			tick_time <- 20;
		}
	}
	
	action release_device(device d) {
		do grade(d);

		invoke release_device(d);
    }
    
    action grade(device d) {
		if (d.condition >= 90) {
			d.grade <- "A";
		} else if (d.condition >= 80) {
			d.grade <- "B";
		} else if (d.condition >= 70) {
			d.grade <- "C";
		} else if (d.condition >= 60) {
			d.grade <- "D";
		} else if (d.condition >= 50) {
			d.grade <- "E";
		} else if (d.condition < 50) {
			d.grade <- "F";
		}
	}
	
	aspect default {
		draw square(10) color: #blue border: #black;
	}
}

species rowa_cell parent: cell {
	init {
		location <- {70,10};
		
		create queue {
			self.size <- 2;
			myself.q <- self;
		}
		
		create station number:1 {
			self.parent_cell <- myself;
			tick_time <- 12;
		}
	}
	
	aspect default {
		draw square(10) color: #purple border: #black;
	}
}


experiment Display type: gui {
    output {
	    display MyDisplay type: java2D {
	        species induction_cell;
	        species rowa_cell;
	        species grading_cell;
	        species cleaning_cell;
	        
	        graphics "path" {
		        loop edges over: floor_graph.edges {
	            	draw geometry(edges) color: #black;
	        	}
        	}
	    }
    }
}

experiment inspect_pie type: gui {	
	output {
   		inspect "Devices" value: device type: table;

   		display "grades" {
   			chart "device_grade_chart" type: pie {
   				data "A" value: device count (each.grade = "A") color: #green;
       			data "B" value: device count (each.grade = "B") color: #blue;
       			data "C" value: device count (each.grade = "C") color: #yellow;
       			data "D" value: device count (each.grade = "D") color: #pink;
       			data "E" value: device count (each.grade = "E") color: #purple;
       			data "F" value: device count (each.grade = "F") color: #red;
   			}
   		}
	}
}

experiment time type: gui {	
	output {
   		display "my_display" {
   			
	        chart "conveyers" type: series {
	        	data "Cleaning Conveyer" value: length(induction_cell accumulate (each.output_conveyer.q.queued_devices)) color: #red;
	        	data "Grading Conveyer" value: length(cleaning_cell accumulate (each.output_conveyer.q.queued_devices)) color: #blue;
	        	data "Rowa Conveyer" value: length(grading_cell accumulate (each.output_conveyer.q.queued_devices)) color: #green;
	        	
	        	data "Induction Queue" value: length(induction_cell accumulate (each.q.queued_devices)) color: #black;
	        	data "Cleaning Queue" value: length(cleaning_cell accumulate (each.q.queued_devices)) color: #chocolate;
	        	data "Cleaning Stations" value: ((cleaning_cell accumulate (each.station)) count (each.state = "processing")) color: #purple;
	        	
	        	data "Finished Devices" value: device count (each.state = "done") color: #orange;
	        }
   		}
	}
}
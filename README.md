# GAMA Demo
This is a simulation of the Verizon CRTC (trade-in phone processing facility) designed to demonstrate the capabilities
of the [Gama simulation framework](https://gama-platform.org/wiki/Home).

## Setting up your environment
Download the latest version of gama provided in the above link and import this experiment into your workpace.

## Prior Art
- POSE: CRTC Simulation Workflow Engine: https://gitlab.com/vznbi/cto/ladybug/pose
- Hive: Simulator/Live Viewer for CRTC: https://gitlab.com/vznbi/cto/ladybug/hive

## Helpful Links
- Gama tutorials: https://gama-platform.org/wiki/LearnGAMLStepByStep
